const HtmlWebPackPlugin = require("html-webpack-plugin");

const htmlPlugin = new HtmlWebPackPlugin({
  template: "./src/index.html",
  filename: "./index.html"
});

module.exports = {
  module: {
    rules: [
      {
        test: /\.json$/,
        use: 'json-loader'
      },
      {
        test:  /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader"
        },        
      },
      {
        test: /\.(png|jpg|woff2|woff|eot|ttf|svg)$/,
        loader: 'file-loader?name=./js/[name]-[hash].[ext]'
      },
      {
        test: /\.css$/,
        use: [
            {  
                loader: "style-loader"  
            },  
            {  
                loader: "css-loader",  
                // options: {  
                // modules: true,  
                // importLoaders: 1,  
                // localIdentName: '[name]__[local]___[hash:base64:5]',  
                // sourceMap: true,  
                // minimize: true  
                // }  
            }  
        ]
      }
    ]
  },
  plugins: [htmlPlugin],
};