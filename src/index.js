import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import createHistory from 'history/createHashHistory';
import { Route, Router, Switch} from 'react-router';
import { store } from './components/store/store.js';
import styles from './components/css/style.css';
import {StoreContext} from 'redux-react-hook'

import Main from './components/Main/Main.jsx';
class Application extends Component {
    
    render() {
        const history = createHistory();
        return (
            <StoreContext.Provider value = {store}>
                <Router history = {history}>
                    <Switch>
                        <Route path="/" component={Main}/>
                    </Switch>
                </Router>
            </StoreContext.Provider >
        );
    }
}



ReactDOM.render(<Application />, document.getElementById("app"));