import React , {useCallback, useEffect} from 'react';
import {Map} from 'immutable';
import {SelectModule} from '../store/actions/TextEditorActions.jsx';
import {ChangeHeigth, ChangeWidth} from '../store/actions/SizeActions.jsx';
import {useDispatch, useMappedState} from 'redux-react-hook';

import TextWrapper from '../Body/Wrapper/TextWrapper/TextWrapper.jsx';
import EditorWrapper from "../Body/Wrapper/EditorWrapper/EditorWrapper.jsx";

const Main = () => {

    const dispatch = useDispatch();
    const mapState = useCallback( state => ({
        modules: state.getIn(['data','modules']) || Map(),
        selectedModuleId: state.getIn(['data','selectedModuleId']),
    }),[modules,selectedModuleId]);

    const {modules, selectedModuleId} = useMappedState(mapState);
    const selectModule = useCallback(moduleId => dispatch(SelectModule(moduleId)));

    const drawModules = () =>{
        const modulesList = modules.map(x => selectedModuleId == x.get('id') ? 
             <TextWrapper opacity={false} data={x} selectModule = {selectModule}/> : <TextWrapper opacity={true} data={x} selectModule = {selectModule}/>);
        return modulesList;
    }
    return(
        <div className='main'>
            <EditorWrapper/>
            <div className='divTest'>
                {drawModules()}
            </div>
            
        </div>
    )
}

export default Main;