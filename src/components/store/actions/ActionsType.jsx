export const EditText = "EDIT_TEXT";
export const EditTitle = "EDIT_TITLE";
export const SelectModule = "SELECT_MODULE";
export const ChangeHeight = "CHANGE_HEIGHT";
export const ChangeWidth = "CHANGE_WIDTH";
export const AddModule = "AddModule";
export const ChangeColor = "CHANGE_COLOR";