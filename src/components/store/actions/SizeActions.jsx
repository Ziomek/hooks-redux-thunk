import * as actions from './ActionsType.jsx';
export function ChangeHeight(height) {
    return {
      type: actions.ChangeHeight,
      height,
    };
  }
  export function ChangeWidth(width) {
    return {
      type: actions.ChangeWidth,
      width,
    };
  }