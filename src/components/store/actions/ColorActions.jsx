import * as actions from './ActionsType.jsx';
export function ChangeColor(color) {
    return {
      type: actions.ChangeColor,
      color,
    };
  }