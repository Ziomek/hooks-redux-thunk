import * as actions from './ActionsType.jsx';
export function AddNewModule(moduleObject) {
    return {
      type: actions.AddModule,
      moduleObject,
    };
  }