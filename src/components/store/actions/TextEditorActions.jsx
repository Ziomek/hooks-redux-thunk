import * as actions from './ActionsType.jsx';

export function EditText(htmlCode) {
    return {
      type: actions.EditText,
      htmlCode,
    };
  }

  export function EditTitle(title) {
    return {
      type: actions.EditTitle,
      title,
    };
  }

  export function SelectModule(moduleId) {
    return {
      type: actions.SelectModule,
      moduleId,
    };
  }