import * as actions from '../actions/ActionsType.jsx';
import {Immutable, Map, fromJS, toJS, List,Iterable,mergeDeep} from 'immutable';
export const styleReducer = (state, action) => {
    switch (action.type) {
      case actions.ChangeHeight:
        let heightState = state;
        
        const heightPath = createPath('height', state);
        const height = action.height;        

        heightState = state.setIn([...heightPath],`${height}px`);
        
        return heightState;
      break;
      case actions.ChangeWidth:
         let widthState = state;
         
         const widthPath = createPath('width', state);
         const width = action.width;        
 
         widthState = state.setIn([...widthPath],`${width}px`);
         
         return widthState;
      break;
      case actions.ChangeColor:
      let colorState = state;
      
      const colorPath = createPath('color', state);
      const color = action.color;        

      colorState = state.setIn([...colorPath],`${color}`);
      
      return colorState;
   break;
    }
    return state;
  }

  const createPath = (value, state) => {
    const selectedModuleId = state.get('selectedModuleId'); 
    const elementIndex = state.get('modules').findIndex(x => x.get('id') === selectedModuleId);
    const path = ['modules',elementIndex,'style',value];  
    return path;
  }