import * as actions from '../actions/ActionsType.jsx';
import {Immutable, Map, fromJS, toJS, List,Iterable,mergeDeep} from 'immutable';
export const moduleReducers = (state, action) => {
    switch (action.type) {
        case actions.AddModule:

          const elementId = state.get('modules').size + 1;
          const moduleObject = action.moduleObject;       
          const newElement = Map({
                id: elementId,
                title:moduleObject.title,
                content:`<p>${moduleObject.content}</p>`,
                style: Map({
                    width:"100px",
                    height:"100px",
                    color:'#FFFFFF',
                })
            });
            let addModuleState = state.update("modules", myList => myList.push(newElement));
          
          return addModuleState;
        break;
      }
      return state;
}