import {dataTemplate} from '../templates/dataTemplate.js';
import {Immutable, Map, fromJS, toJS, List,Iterable,mergeDeep} from 'immutable';
import * as actions from '../actions/ActionsType.jsx';
export const dataReducer = (state, action) => {
  switch (action.type) {
    case actions.EditText:
      let contentState = state;
      const selectedModuleIdContent = state.get('selectedModuleId');   
      const content = action.htmlCode.toString('html');
      let elementIndexContent = state.get('modules').findIndex(x => x.get('id') === selectedModuleIdContent);     
      const pathContent = ['modules',elementIndexContent,'content'];

      contentState = state.setIn([...pathContent],content);
      
      return contentState;
    break;
    case actions.EditTitle:
      let titleState = state;
      const selectedModuleIdTitle = state.get('selectedModuleId');
      let elementIndexTitle = state.get('modules').findIndex(x => x.get('id') === selectedModuleIdTitle);     
      const pathTitle = ['modules',elementIndexTitle,'title'];

      titleState = state.setIn([...pathTitle],action.title);
      
      return titleState;
    break;
    case actions.SelectModule:
        return state.set('selectedModuleId',action.moduleId);
    break;

  }
  if (typeof state === 'undefined') {
    state = dataTemplate;
  }
  return state;
}