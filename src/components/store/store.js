import {applyMiddleware, createStore, combineReducers, compose} from 'redux';
import thunk from 'redux-thunk';
import {Iterable,toJS} from 'immutable';
import { createLogger } from 'redux-logger';
import { routerReducer} from 'react-router-redux'
import {dataReducer} from './reducers/dataReducers.js';
import {styleReducer} from './reducers/StyleReducers.jsx';
import {defaultState} from './templates/defaultState.js';
import {moduleReducers} from './reducers/moduleReducers.jsx';
const reducers = combineReducers
({
  data: dataReducer
});
export const rootReducer = (rootState = defaultState, action) => {
    rootState = rootState.set('data', dataReducer(rootState.get('data'),action));
    rootState = rootState.set('data', styleReducer(rootState.get('data'),action));
    rootState = rootState.set('data', moduleReducers(rootState.get('data'),action));
    return rootState;
}

const asyncDispatchMiddleware = store => next => action => {
    let syncActivityFinished = false;
    let actionQueue = [];
  
    flushQueue = () => {
      actionQueue.forEach(a => store.dispatch(a)); // flush queue
      actionQueue = [];
    }
  
    asyncDispatch = (asyncAction) => {
      actionQueue = actionQueue.concat([asyncAction]);
  
      if (syncActivityFinished) {
        flushQueue();
      }
    }
  
    const actionWithAsyncDispatch =
          Object.assign({}, action, { asyncDispatch });
  
      next(actionWithAsyncDispatch);
      syncActivityFinished = true;
      flushQueue();
    };
  
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  // Enable Logging Middleware only in Development mode
  let tmpStore;
  let middlewares = []
  if (process.env.NODE_ENV === 'development') {
      tmpStore = createStore(rootReducer, composeEnhancers(applyMiddleware(createLogger({
        duration: true,
        timestamp: true,
        stateTransformer: (rootState) => {
          if (Iterable.isIterable(rootState)) return rootState.toJS();
          else return rootState;
        }
    }), thunk)));
  } else {
      tmpStore = createStore(rootReducer, applyMiddleware(thunk));
  }
  
  export const store = tmpStore;