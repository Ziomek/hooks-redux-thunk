import {Immutable, Map, fromJS, toJS, List,Iterable,mergeDeep} from 'immutable';

export const dataTemplate = Map({
    modules: List([
        Map({
            id:1,
            title:"Module 1",
            content:"<p>Module 1</p>",
            style: Map({
                width:"100px",
                height: "100px",
                color:'#FFFFFF',
            })
        }),
        Map({
            id:2,
            title:"Module 2",
            content:"<p>Module 2</p>",
            style: Map({
                width: "600px",
                height: "100px",
                color:'#FFFFFF',
            })
        }),
    ]),
    selectedModuleId: 0,    
});