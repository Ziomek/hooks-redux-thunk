import React from 'react'
import TextModule from '../../Modules/TextModule/TextModule.jsx';
import TitleModule from '../../Modules/TitleModule/TitleModule.jsx';
const TextWrapper = (props) =>{ 
    const opacity = props.opacity ? "0.5" : "1";
    return(
        <section id={props.data.get('id')} className='textWrapper' style={{width:props.data.getIn(['style', 'width']), opacity:opacity}} onClick={() => props.selectModule(props.data.get('id'))}>
            <TitleModule text={props.data.get('title')}/>
            <TextModule htmlCode={props.data.get('content')} height={props.data.getIn(['style', 'height'])}  color={props.data.getIn(['style', 'color'])}/>
        </section>
    );
}
export default TextWrapper;