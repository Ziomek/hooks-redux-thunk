import React, {useCallback} from 'react'
import TextEditorModule from "../../../Body/Modules/TextEditorModule/TextEditorModule.jsx";
import SizeModule from "../../../Body/Modules/StyleModule/SizeModule/SizeModule.jsx";
import ColorModule from "../../../Body/Modules/StyleModule/ColorModule/ColorModule.jsx";
import {useDispatch, useMappedState} from 'redux-react-hook'
import {ChangeHeight, ChangeWidth} from '../../../store/actions/SizeActions.jsx';
import {EditText, EditTitle} from '../../../store/actions/TextEditorActions.jsx';
import AddModule from "../../../Body/Modules/AddModule/AddModule.jsx";
import InputEditor from "../../Modules/InputEditor/InputEditor.jsx";
const EditorWrapper = () => {

    const dispatch = useDispatch();

    const mapState = useCallback( state => ({
        selectedModule: state.getIn(['data','selectedModuleId']),
        modules: state.getIn(['data','modules']),
    }),[selectedModule]);

    const {modules, selectedModule} = useMappedState(mapState); 

    const editText = useCallback(htmlCode => dispatch(EditText(htmlCode)));
    const editTitle = useCallback(title => dispatch(EditTitle(title)));
    const changeHeight = useCallback(height => dispatch(ChangeHeight(height)));
    const changeWidth = useCallback(width => dispatch(ChangeWidth(width)));

    const getContentOfSelectedModule = () =>{
        if(selectedModule == null || modules == null || selectedModule == 0)
            return "";
        const element = modules.filter(x => x.get('id') === selectedModule).get(0);        
        return element.get('content');
    }

    const getTitleOfSelectedModule = () =>{
        if(selectedModule == null || modules == null || selectedModule == 0)
            return "";
        const element = modules.filter(x => x.get('id') === selectedModule).get(0);        
        return element.get('title');
    }

    const getSizeOfSelectedElement = () =>{
        if(selectedModule == null || modules == null || selectedModule == 0)
            return {};
        const element = modules.filter(x => x.get('id') === selectedModule).get(0);   
        const size = {};
        size.height = element.getIn(['style','height']);
        size.width = element.getIn(['style','width']);   
        return size;
    }

    const getColorOfSelectedElement = () =>{
        if(selectedModule == null || modules == null || selectedModule == 0)
            return {};
        const element = modules.filter(x => x.get('id') === selectedModule).get(0); 
        return element.getIn(['style','color']);
    }

    return(
        <section className="editorWrapper">
            <TextEditorModule editText={editText} content={getContentOfSelectedModule()} selectedModule={selectedModule}/>
            <SizeModule size={getSizeOfSelectedElement()}  changeHeight={changeHeight} changeWidth={changeWidth}/>
            <ColorModule _color={getColorOfSelectedElement()}/>
            <InputEditor _value={getTitleOfSelectedModule()} editFunc={editTitle}/>
            <AddModule/>
        </section>
    )
}

export default EditorWrapper;