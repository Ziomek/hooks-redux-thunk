import React, {useState, useEffect} from "react";

const InputEditor = ({_value, editFunc}) =>{
    const [value,setValue] = useState(_value);

    const onChange = (e) =>{
        setValue(e.target.value);
        editFunc(e.target.value);
    }

    useEffect(()=>{
        setValue(_value);
    },[_value]);

    return(        
        <input value={value} onChange={onChange}/>
    );

};
export default InputEditor;