import React from "react";

const TitleModule = (props) =>{
    return(
        <section className="titleModule">
            <h1>{props.text}</h1>
        </section>
    );
}

export default TitleModule;