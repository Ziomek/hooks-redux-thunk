import React, {useState, useEffect} from 'react'
import RichTextEditor from 'react-rte';
const TextEditorModule = ({editText,selectedModule,content}) =>{

    const [value, setValue] = useState(RichTextEditor.createEmptyValue());

    const onChange = (newValue) => {
        setValue(newValue)
        editText(newValue);
    };

    const cleanRichBox = () =>{
        setValue(RichTextEditor.createEmptyValue());
    };

    const setTextFromModel = () =>{
        setValue(RichTextEditor.createValueFromString(content,'html'));   
    };
          
    useEffect(()=>{                
        cleanRichBox();
        setTextFromModel();
    },[selectedModule]);

    return(
        <div className='textEditorModule'>
            <RichTextEditor
                value={value}
                onChange={onChange}
            />
        </div>
    )
}

export default TextEditorModule;
