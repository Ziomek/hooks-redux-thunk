import React, {useCallback} from "react";
import {useDispatch, useMappedState} from 'redux-react-hook';
import {AddNewModule} from "../../../store/actions/ModuleActions.jsx";
const AddModule = () =>{

    const dispatch = useDispatch();
    const addModule = useCallback( moduleObject => dispatch(AddNewModule(moduleObject)));

    const addNewModule = () => {
        const moduleObject = {};
        moduleObject.title="test";
        moduleObject.content="test content";
        addModule(moduleObject);
    }

    return(
        <React.Fragment>
            <button onClick={addNewModule} >ClickMe</button>
        </React.Fragment>
    )
}

export default AddModule;