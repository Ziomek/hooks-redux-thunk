import React, {useState, useEffect} from 'react'

const SizeModule = ({changeHeight, changeWidth, size}) =>{
    const parsedheight = parseInt(size.height);
    const parsedWidth= parseInt(size.width);
    
    const [height, setheight] = useState(0);
    const [width, setWidth] = useState(0);
    
    useEffect(()=>{
        if(height != parsedheight)
            setheight(parsedheight);
        if(width != parsedWidth)
            setWidth(parsedWidth);
    })

    const onChangeHeight = (e) =>{
        const height = e.target.value;
        setheight(height)
        changeHeight(height);
    }

    const onChangeWidth = (e) =>{
        const width = e.target.value;
        setWidth(width)
        changeWidth(width);
    }

    return(
        <div className="sizeModule">
            <input type="number" value={height} onChange={ (e) => onChangeHeight(e)}/>
            <input type="number" value={width} onChange={ (e) => onChangeWidth(e)}/>
        </div>
    )
}
export default SizeModule;