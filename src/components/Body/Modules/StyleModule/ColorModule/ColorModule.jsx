import React, {useState, useCallback, useEffect} from "react"
import { ChromePicker } from 'react-color'
import {ChangeColor} from '../../../../store/actions/ColorActions.jsx';
import {useDispatch} from 'redux-react-hook';
const ColorModule = ({_color}) =>{
    const dispatch = useDispatch();
    const [color, setColor] = useState(_color);
    const colorChange = useCallback(color => dispatch(ChangeColor(color)));
    const changeColor = (color) =>{
        colorChange(color.hex);
    }
    useEffect(()=>{
        setColor(_color);
    },[_color])
    return(
        <ChromePicker color={color} onChangeComplete={changeColor}/>
    )
}
export default ColorModule;