import React, {useState, useEffect} from 'react';

const NumberEditorModule = ({_value, editFunc}) =>{

    const [value, setValue] = useState(_value);

    const onChange = (e) =>{
        const elementValue = e.target.value;
        setValue(elementValue);
        editFunc(elementValue);
    }

    useEffect(()=>{
        setValue(_value);
    },[_value])

    return(
        <input type='number' value={value} onChange={onChange}/>
    );
};

export default NumberEditorModule;