import React from 'react'

const TextModule = ({height, htmlCode, color}) =>{
    console.log(color);
    return(
        <div className='textModule' style={{height:height, backgroundColor:color}}  dangerouslySetInnerHTML={{ __html: htmlCode }}  />
    );
}
export default TextModule;