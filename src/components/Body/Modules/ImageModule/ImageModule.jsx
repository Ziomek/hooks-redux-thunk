import React from 'react'

const ImageModule = (img) =>{
    return(
        <React.Fragment>
            <img src={img}></img>
        </React.Fragment>
    );
}
export default ImageModule;